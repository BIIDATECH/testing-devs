
**API para administrar notas y usuarios**

Campos de usuarios:
* Nombre   
* email   
* usuario  
* password


Campos de las notas: 

* Fecha 
* Titulo 
* Descripcion
* Tags


En la relación entre usuarios y notas es de uno a muchos (Un usuario tiene N notas)

Para consultar las notas se debe consultar de manera segura (Mediante token JWT)

API con lo sguientes endpoints:

Secciones publicas:
* /register
* /login


Secciones con seguridad:
* /notas/all
* /notas/get-by-id
* /notas/get-by-title
* /notas/insert
* notas/update
* notas/delete




EL backend se puede hacer en:

* ExpressJs (NodeJS)
* Spring Boot (Java)